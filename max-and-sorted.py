from statistics import mean


class Student:
    """Sa docstring, comme pour une
    fonction.
    """

    def __init__(self, name):
        self.name = name
        self.notes = []

    def add_exam(self, mark):
        self.notes.append(mark)

    def get_mean(self):
        return mean(self.notes)

    def __repr__(self):
        return f"Student(name={self.name!r}, marks={self.notes!r})"


def by_mean(etudiant):
    return etudiant.get_mean()


s1 = Student("Ada")
s1.add_exam(10)

s2 = Student("Alan")
s2.add_exam(20)

s3 = Student("John")
s3.add_exam(0)

print(max([s1, s2, s3], key=by_mean))
print(max([s1, s2, s3], key=lambda student: student.get_mean()))

print(sorted([s1, s2, s3], key=by_mean))
print(sorted([s1, s2, s3], key=lambda student: student.name))
