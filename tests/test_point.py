from math import isclose

from hypothesis import given
from hypothesis.strategies import floats

from point import Point


def test_origin_is_at_zero():
    origin = Point(0, 0)
    assert origin.dist_from_origin() == 0


bound = 2**128


@given(floats(allow_nan=False, min_value=-bound, max_value=bound))
def test_not_origin_is_not_at_zero(x):
    if isclose(x, 0, abs_tol=0.001):
        return
    not_origin = Point(x, 0)
    assert not_origin.dist_from_origin() > 0


@given(
    floats(allow_nan=False, min_value=-bound, max_value=bound),
    floats(allow_nan=False, min_value=-bound, max_value=bound),
)
def test_dist(x, y):
    origin = Point(0, 0)
    p1 = Point(x, y)
    assert origin.dist(p1) == p1.dist(origin)
