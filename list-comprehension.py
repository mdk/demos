# Liste en compréhension
# Liste en intension (!= extension)

[1, 2, 3]
print([i for i in range(0, 100) if i % 3 == 0 or i % 5 == 0])

ensemble_en_extension = {1, 2, 3}
ensemble_en_intension = {i for i in range(0, 100) if i % 3 == 0 or i % 5 == 0}

{1: 1, 2: 2, 3: 3}
print({i: i**2 for i in range(0, 100) if i % 3 == 0 or i % 5 == 0})


recettes = [
    {"durée": 10},
    {"durée": 12},
    {"durée": 14},
    {"durée": 60},
]

durées = [recette["durée"] for recette in recettes]
