from dataclasses import dataclass
import json
import math


def mul(init, *args):
    """Multiply all given arguments.

    >>> mul(3, 3)
    9
    >>> mul(3, 3, 3)
    27
    """
    total = init
    for arg in args:
        total *= arg  # total = total * arg
    return total


@dataclass
class Report:
    date: str
    current: int
    last: int
    total: int


api_result = """
{
    "date": "2023-12-13",
    "current": 123,
    "last": 118,
    "total": 12
}
"""


api_data = json.loads(api_result)
report = Report(**api_data)
print(report)
print("Difference:", abs(report.last - report.current))


def test_mul():
    assert mul() == 1
    assert mul(3) == 3
    assert mul(2, 3) == 6
    assert mul(2, 3, 2) == 12
    assert mul(123456, 654321, 12) == mul(654321, 12, 123456)
