"""Demo implementation of a simple Point class."""


class Point:
    """A 2D point."""

    def __init__(self, x: float, y: float):  # Dunder init "Double underscore init"
        self.x = x
        self.y = y

    def __repr__(self):
        return f"Point(x={self.x}, y={self.y})"

    def dist_from_origin(self) -> float:
        """Compute distance between self and origin."""
        return (self.x**2 + self.y**2) ** 0.5

    def dist(self, other) -> float:
        """Compute distance between two points."""
        return ((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5
