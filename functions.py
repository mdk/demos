def is_even(n):
    """Returns True if n is even.

    >>> is_even(10)
    True
    """
    return n % 2 == 0


def is_odd(n):
    """Returns True if n is odd.

    >>> is_odd(10)
    False
    """
    return not is_even(n)


for i in range(10):
    if is_even(i):
        print(f"{i} est pair")
    if is_odd(i):
        print(f"{i} est impair")
