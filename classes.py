# Une classe sert à stocker des attributs ensemble.
from dataclasses import dataclass
from statistics import mean
from matplotlib import pyplot as plt
import datetime as dt
from random import randint


@dataclass
class Point:
    x: float
    y: float


origin = Point(0, 0)  # origin.x = 0, origin.y = 0


class Dice:
    def __init__(self, value=6):
        self.value = value

    def throw(self):
        self.value = randint(1, 6)

    def __repr__(self):
        return " ⚀⚁⚂⚃⚄⚅"[self.value]


class DiceCup:
    def __init__(self, dices):
        self.dices = dices

    def shake(self):
        for dice in self.dices:
            dice.throw()

    def total(self):
        return sum(dice.value for dice in self.dices)

    def mean(self):
        return mean(dice.value for dice in self.dices)


def show_graph():
    cup = DiceCup([Dice() for _ in range(1000)])
    results = {}
    for _ in range(10_000):
        cup.shake()
        try:
            results[cup.total()] = results[cup.total()] + 1
        except KeyError:
            results[cup.total()] = 1

    plt.scatter(results.keys(), results.values())
    plt.show()


# def dist_from_origin(x, y):
#     """Compute distance between a point and the origin."""
#     return (x**2 + y**2) ** 0.5
#
#
# def dist(x1, y1, x2, y2):
#     """Compute distance between two points."""
#     return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
#
#
# def dist_from_origin(point):
#     """Compute distance between a point and the origin."""
#     return (point["x"] ** 2 + point["y"] ** 2) ** 0.5
#
#
# def dist(p1, p2):
#     """Compute distance between two points."""
#     return ((p1["x"] - p2["x"]) ** 2 + (p1["y"] - p2["y"]) ** 2) ** 0.5


def dist_from_origin(point):
    """Compute distance between a point and the origin."""
    return (point.x**2 + point.y**2) ** 0.5


def dist(p1, p2):
    """Compute distance between two points."""
    return ((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2) ** 0.5


@dataclass
class Recette:
    nom: str
    ingrédients: dict


@dataclass
class Report:
    date: dt.datetime
    nb_last_month: int
    nb_current_month: int
    total_values: float

    def update(self):
        ...

    def save(self, path):
        ...


def load_report(path):
    return Report(dt.date.today(), 0, 0, 0)


def update_report(report):
    ...


# def compare_reports(
#     date,
#     nb_last_month,
#     nb_current_month,
#     total_values,
#     other_date,
#     other_nb_last_month,
#     other_nb_current_month,
#     other_total_values,
# ):
#     ...


def compare_reports(report, other_report):
    ...


def save_report(path, report):
    ...


daily_report = load_report("daily.xls")
monthly_report = load_report("monthly.xls")
update_report(daily_report)
update_report(monthly_report)
save_report("daily.xls", daily_report)
save_report("monthly.xls", monthly_report)


daily_report = load_report("daily.xls")
monthly_report = load_report("monthly.xls")
daily_report.update()
monthly_report.update()
daily_report.save("daily.xls")
monthly_report.save("monthly.xls")
